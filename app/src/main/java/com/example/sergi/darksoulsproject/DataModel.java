package com.example.sergi.darksoulsproject;

/**
 * Created by Scud on 07/02/2018.
 */

public class DataModel {

    String name;
    String zone;
    String difficulty;
    String drops;
    String imagesrc;
    String extrainfo;
    String zone_boss;
    String zone_enemy;
    String zone_npc;
    String items;


    public DataModel(String name, String zone, String difficulty, String drops,String extrainfo, String imagesrc ) {
        this.name=name;
        this.zone=zone;
        this.difficulty=difficulty;
        this.drops=drops;
        this.extrainfo=extrainfo;
        this.imagesrc=imagesrc;

    }
    public DataModel(String name, String zone_npc, String zone_boss, String zone_enemy,String items, String imagesrc, String aux ) {
        this.name=name;
        this.zone_npc=zone_npc;
        this.zone_boss=zone_boss;
        this.zone_enemy=zone_enemy;
        this.imagesrc=imagesrc;
        this.items=items;

    }


    public String getName() {
        return name;
    }


    public String getZone() {
        return zone;
    }


    public String getDifficulty() {
        return difficulty;
    }


    public String getDrops() {
        return drops;
    }
    public String getExtrainfo() {
        return extrainfo;
    }
    public String getItems() {
        return items;
    }

    public String getImagesrc() {
        return imagesrc;
    }
    public String getZone_boss() {
        return zone_boss;
    }
    public String getZone_npc() {
        return zone_npc;
    }
    public String getZone_enemy() {
        return zone_enemy;
    }



}
