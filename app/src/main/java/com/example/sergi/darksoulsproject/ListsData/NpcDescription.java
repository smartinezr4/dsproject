package com.example.sergi.darksoulsproject.ListsData;

import android.content.res.Resources;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.sergi.darksoulsproject.R;

/**
 * Created by Scud on 13/02/2018.
 */

public class NpcDescription extends AppCompatActivity {
    TextView txtName, txtLocation, txtDrop, txtExtrainfo;
    ImageView imgView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.description_activity);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        String name = getIntent().getStringExtra("name");
        String location = getIntent().getStringExtra("location");
        String drop = getIntent().getStringExtra("drop");
        String extrainfo = getIntent().getStringExtra("extrainfo");
        String imgsrc = getIntent().getStringExtra("imgsrc");
        txtName = findViewById(R.id.txttitol);
        txtName.setText(name);
        txtLocation = findViewById(R.id.txtlocation);
        txtLocation.setText(location);
        txtDrop = findViewById(R.id.txtboss);
        txtDrop.setText(drop);
        txtExtrainfo = findViewById(R.id.txtextrainfo);
        txtExtrainfo.setText(extrainfo);
        imgView = findViewById(R.id.imgsrc);
        Resources res = getResources();
        imgView.setImageResource(res.getIdentifier(imgsrc,"drawable",getPackageName()));

    }
}
