package com.example.sergi.darksoulsproject;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import com.example.sergi.darksoulsproject.DBTools.Boss;
import com.example.sergi.darksoulsproject.DBTools.Contact;
import com.example.sergi.darksoulsproject.DBTools.DatabaseHandler;
import com.example.sergi.darksoulsproject.ListsData.BossList;
import com.example.sergi.darksoulsproject.ListsData.NpcList;
import com.example.sergi.darksoulsproject.ListsData.ZoneList;

import java.util.List;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    Button btnBosses;
    Button btnNPCs;
    Button btnZones;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nav_bar);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);


        btnBosses = findViewById(R.id.btnBosses);
        btnBosses.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // Perform action on click
                Intent activityChangeIntent = new Intent(MainActivity.this, BossList.class);

                // currentContext.startActivity(activityChangeIntent);

                MainActivity.this.startActivity(activityChangeIntent);
            }
        });
        btnNPCs = findViewById(R.id.btnNPCs);
        btnNPCs.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // Perform action on click
                Intent activityChangeIntent = new Intent(MainActivity.this, NpcList.class);

                // currentContext.startActivity(activityChangeIntent);

                MainActivity.this.startActivity(activityChangeIntent);
            }
        });
        btnZones = findViewById(R.id.btnZones);
        btnZones.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // Perform action on click
                Intent activityChangeIntent = new Intent(MainActivity.this, ZoneList.class);

                // currentContext.startActivity(activityChangeIntent);

                MainActivity.this.startActivity(activityChangeIntent);
            }
        });

        //DB Stuff
        DatabaseHandler db = new DatabaseHandler(this);

        /**
         * CRUD Operations
         * */
        // Inserting Bosses
        /*Log.d("Insert: ", "Inserting ..");
        db.addBoss(new Boss("Asylum DemonDB", "Undead Asylum", "Easy", "Drops: 2,000 souls, 1 Humanity, Big Pilgrim's Key, Demon's Great Hammer", "Large demon found during the tutorial in the Undead Asylum. This boss is very similar to the vanguard in Demon's Souls in both attack style and appearance in the story. After escaping from your cell and lighting the first bonfire, you will open a large set of doors leading to another courtyard surrounded by large vases.", "asylumdemon" ));
        db.addBoss(new Boss("Taurus DemonDB", "Undead Burg", "Easy", "Drops: 3,000 souls, 1 Humanity, 1 Homeward Bone, Demon's Greataxe (rare)", "A large demon found on top of a narrow castle wall in the Undead Burg, on the road to the Undead Parish. Similar to the Bell Gargoyles encountered later, the Taurus Demon does not immediately appear when the host passes through the fog gate.", "taurus"));
        db.addBoss(new Boss("Bell GargoyleDB", "Undead Burg", "Medium", "Drops: 10,000 souls, 1 Twin Humanity, Gargoyle Tail Axe (cut tail), Gargoyle's Halberd (rare), Gargoyle Shield (rare), Gargoyle Helm (rare)", "Located on the roof of the Undead Parish, guarding the path to one of the Bells of Awakening. Not just one, but two of these axe-wielding, fire breathing demons must be killed for the player to proceed.", "gargoyle"));
        db.addBoss(new Boss("Capra DemonDB", "Undead Burg", "Easy", "Drops: 6,000 souls, 1 Humanity, 1 Homeward Bone, Demon Great Machete (rare)", "The Capra Demon is the boss of the lower section of the Undead Burg. You can get to him either by going left down to the end of the main street (past the Undead Thieves and Poison Dogs), or by going through the shortcut in the waterway in the Firelink Shrine after you unlock it.", "capra"));
        *///db.addBoss(new Boss("", "", "", "", "", ""));

        //Reading Bosses
        Log.d("Reading: ", "Reading all bosses...");
        List<Boss> bosses = db.getAllBosses();

        for (Boss b : bosses) {
            String log = "Id: " + b.get_id() + " , Name: " + b.get_name() + " , Zone: " + b.get_zone() + " , Difficulty: " + b.get_difficulty() + " , Drops: " + b.get_drops() + " , Extrainfo: " + b.get_extrainfo() + " , Imatge: " + b.get_imagesrc();
            //Writing bosses to log
            Log.d("Boss: ", log);
        }
        //End DB Stuff

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.nav_bar, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //Afegir nou registre
        if (id == R.id.action_addNewBoss) {
            Intent activityChangeIntent = new Intent(MainActivity.this, AddBossActivity.class);
            MainActivity.this.startActivity(activityChangeIntent);

        } else if (id == R.id.action_addNewNpc) {
            Intent activityChangeIntent = new Intent(MainActivity.this, NpcList.class);
            MainActivity.this.startActivity(activityChangeIntent);

        } else if (id == R.id.action_addNewZone) {
            Intent activityChangeIntent = new Intent(MainActivity.this, ZoneList.class);
            MainActivity.this.startActivity(activityChangeIntent);

        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.bosses_activity) {
            Intent activityChangeIntent = new Intent(MainActivity.this, BossList.class);
            MainActivity.this.startActivity(activityChangeIntent);

        } else if (id == R.id.zones_activity) {
            Intent activityChangeIntent = new Intent(MainActivity.this, ZoneList.class);
            MainActivity.this.startActivity(activityChangeIntent);

        } else if (id == R.id.npcs_activity) {
            Intent activityChangeIntent = new Intent(MainActivity.this, NpcList.class);
            MainActivity.this.startActivity(activityChangeIntent);

        } else if (id == R.id.weapons_activity) {

        } else if (id == R.id.stats_activity) {

        } else if (id == R.id.lore_activity) {

        } else if (id == R.id.mechanics_activity) {

        } else if (id == R.id.others_activity) {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}