package com.example.sergi.darksoulsproject.DBTools;

/**
 * Created by sergi on 20-Feb-18.
 */

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by sergi on 20-Feb-18.
 */

public class DatabaseHandler extends SQLiteOpenHelper {

    //All Static variables
    //Database Version
    private static final int DATABASE_VERSION = 2;


    //DB Name
    private static final String DATABASE_NAME = "dswiki";

    //tables names
    private static final String TABLE_BOSSES = "bosses";

    //bosses table columns names
    private static final String KEY_ID = "id";
    private static final String KEY_NAME = "name";
    private static final String KEY_ZONE = "zone";
    private static final String KEY_DIFFICULTY = "difficulty";
    private static final String KEY_DROPS = "drops";
    private static final String KEY_EXTRAINFO = "extrainfo";
    private static final String KEY_IMAGESRC = "imagesrc";

    public DatabaseHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }


    //Creating tables
    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_BOSSES_TABLE = "CREATE TABLE " + TABLE_BOSSES + "("
                + KEY_ID + " INTEGER PRIMARY KEY,"
                + KEY_NAME + " TEXT,"
                + KEY_ZONE + " TEXT,"
                + KEY_DIFFICULTY + " TEXT,"
                + KEY_DROPS + " TEXT,"
                + KEY_EXTRAINFO + " TEXT,"
                + KEY_IMAGESRC + " TEXT"
                + ")";
        db.execSQL(CREATE_BOSSES_TABLE);

    }

    //Upgrading database
    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {
        //Drop older table if it exists

        db.execSQL("DROP TABLE IF EXISTS " + TABLE_BOSSES);

        //Create tables again
        onCreate(db);
    }

    //Adding new contact
    public void addBoss(Boss boss) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_NAME, boss.get_name()); //boss name
        values.put(KEY_ZONE, boss.get_zone()); //boss zone
        values.put(KEY_DIFFICULTY, boss.get_difficulty()); //boss difficulty
        values.put(KEY_DROPS, boss.get_drops()); //boss drops
        values.put(KEY_EXTRAINFO, boss.get_extrainfo()); //boss extrainfo
        values.put(KEY_IMAGESRC, boss.get_imagesrc()); //boss imagesrc
        //Insert Row
        db.insert(TABLE_BOSSES, null, values);
        db.close(); //Close database connection

    }

    // Getting single boss
    public Boss getBoss(int id) {
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(TABLE_BOSSES, new String[] {KEY_ID,
                        KEY_NAME, KEY_ZONE, KEY_DIFFICULTY, KEY_DROPS, KEY_EXTRAINFO, KEY_IMAGESRC }, KEY_ID + "=?",
                new String[] { String.valueOf(id)}, null, null, null,null);
        if (cursor != null)
            cursor.moveToFirst();

        Boss boss = new Boss(Integer.parseInt(cursor.getString(0)),
                cursor.getString(1),
                cursor.getString(2),
                cursor.getString(3),
                cursor.getString(4),
                cursor.getString(5),
                cursor.getString(6));

        //cursor.close();
        //return boss
        return boss;

    }

    // Getting All Bosses
    public List<Boss> getAllBosses() {
        List<Boss> bossList = new ArrayList<Boss>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_BOSSES;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Boss boss = new Boss();
                boss.set_id(Integer.parseInt(cursor.getString(0)));
                boss.set_name(cursor.getString(1));
                boss.set_zone(cursor.getString(2));
                boss.set_difficulty(cursor.getString(3));
                boss.set_drops(cursor.getString(4));
                boss.set_extrainfo(cursor.getString(5));
                boss.set_imagesrc(cursor.getString(6));
                // Adding boss to list
                bossList.add(boss);
            } while (cursor.moveToNext());
        }
        cursor.close();

        // return boss list
        return bossList;
    }

    // Getting bosses Count
    public int getBossesCount() {
        String countQuery = "SELECT  * FROM " + TABLE_BOSSES;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        cursor.close();

        // return count
        return cursor.getCount();
    }
    // Updating single Boss
    public int updateBoss(Boss boss) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_NAME, boss.get_name()); //boss name
        values.put(KEY_ZONE, boss.get_zone()); //boss zone
        values.put(KEY_DIFFICULTY, boss.get_difficulty()); //boss difficulty
        values.put(KEY_DROPS, boss.get_drops()); //boss drops
        values.put(KEY_EXTRAINFO, boss.get_extrainfo()); //boss extrainfo
        values.put(KEY_IMAGESRC, boss.get_imagesrc()); //boss imagesrc

        // updating row
        return db.update(TABLE_BOSSES, values, KEY_ID + " = ?",
                new String[] { String.valueOf(boss.get_id()) });
    }

    // Deleting single boss
    public void deleteBoss(Boss boss) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_BOSSES, KEY_ID + " = ?",
                new String[] { String.valueOf(boss.get_id()) });
        db.close();
        Log.d("Boss: ","Removed.");
    }

}
