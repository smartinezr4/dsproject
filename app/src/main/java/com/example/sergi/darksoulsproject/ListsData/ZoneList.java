package com.example.sergi.darksoulsproject.ListsData;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.example.sergi.darksoulsproject.CustomAdapter;
import com.example.sergi.darksoulsproject.DataModel;
import com.example.sergi.darksoulsproject.R;

import java.util.ArrayList;

/**
 * Created by Scud on 09/02/2018.
 */

public class ZoneList extends AppCompatActivity {
    ArrayList<DataModel> dataModels;
    ListView listView;
    public static CustomAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.content_main2);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        listView = findViewById(R.id.list);

        dataModels = new ArrayList<>();

        dataModels.add(new DataModel("Northern Undead Asylum",
                "NPCS in the area\n" +
                        "· Elite Knight Oscar\n" +
                        "· Snuggly\n\n" ,

                        "Bosses: \n" +
                        "· Asylum Demon\n\n" ,

                        "Enemies\n" +
                        "· Zombie\n" +
                        "· Undead",

                "Items\n" +
                        "· Dungeon Cell Key\n" +
                        "· Estus Flask\n" +
                        "· Starting Class' weapon\n" +
                        "· Undead A. F2 East Key\n" +
                        "· Big Piligrim's Key\n" +
                        "· Demon's Great Hammer\n" +
                        "· Humanity Item\n" +
                        "· Soul of a Lost Undead",

                "zonesnorthernundeadasylum",""));

        dataModels.add(new DataModel("Firelink Shrine",
                "NPCS in the area\n" +
                        "· Anastacia of Astora\n" +
                        "· Crestfallen Warrior\n" +
                        "· Petrus of Thorolund\n" +
                        "· Reah of Thorolund\n" +
                        "· Vince of Thorolund\n" +
                        "· Nico of Thorlound\n" +
                        "· Knight Lautrec of Carim\n" +
                        "· Griggs of Vinheim\n" +
                        "· Big Hat Logan\n" +
                        "· Laurentius of the Great Swamp\n" +
                        "· Kingseeker Frampt\n\n" ,

                        "Bosses: \n" +
                        "· -\n\n" ,

                        "Enemies\n" +
                        "· Armored Zombie\n" +
                        "· Undead Soldier\n" +
                        "· Skeleton\n" +
                        "· Giant Skeleton\n" +
                        "· Sewer Rat",

                "Items\n" +
                        "· Humanity\n" +
                        "· Soul of a Lost Undead\n" +
                        "· Cracked Red Eye Orb\n" +
                        "· Morning Star & Talisman\n" +
                        "· Homeward Bone\n" +
                        "· Lloyd's Talisman\n" +
                        "· Large Soul of a Lost Undead\n" +
                        "· Zweihander\n" +
                        "· Winged Spear\n" +
                        "· Caduceus Round Shield\n" +
                        "· Binoculars\n" +
                        "· Ring of Sacrifice\n" +
                        "· Firebomb\n" +
                        "· Undead A. West F2 Tower Key",

                "zonesfirelinkshrine",""));

        dataModels.add(new DataModel("Undead Burg",
                "NPCS in the area\n" +
                        "· Undead Merchant\n" +
                        "· Solaire of Astora\n\n" ,

                        "Bosses: \n" +
                        "· Black Knight\n" +
                        "· Havel The Rock\n" +
                        "· The Bridge Wyvern\n" +
                        "· Taurus Demon\n\n" ,

                        "Enemies\n" +
                        "· Armored Zombie\n" +
                        "· Undead Soldier\n" +
                        "· Armoured Undead Archer\n" +
                        "· Sewer Rats",

                "Items\n" +
                        "· White Sign Soapstone\n" +
                        "· Gold Pine Resin\n" +
                        "· Light Crossbow\n" +
                        "· Standard Bold\n" +
                        "· Blue Tearstone Ring\n" +
                        "· Soul of a Lost Undead\n" +
                        "· Humanity\n" +
                        "· Rubbish\n" +
                        "· Black Firebomb\n" +
                        "· Throwing Knife\n" +
                        "· Wooden Shield\n" +
                        "· Titanite Chunk\n" +
                        "· Drake Sword",

                "zonesundeadburg",""));

        dataModels.add(new DataModel("Undead Parish",
                "NPCS in the area\n" +
                        "· Laurentius of the Great Swamp\n" +
                        "· Domhnall of Zena\n" +
                        "· Solaire of Astora\n" +
                        "· Knight Lautrec\n\n" ,

                        "Bosses: \n" +
                        "· Butcher\n" +
                        "· Giant Rat\n" +
                        "· Knight Kirk\n" +
                        "· The Channeler\n" +
                        "· Gaping Dragon\n\n" ,

                        "Enemies\n" +
                        "· Zombie\n" +
                        "· Torch Zombie\n" +
                        "· Undead Attack Dog\n" +
                        "· Slime\n" +
                        "· Sewer Rat\n" +
                        "· Basilisk",

                "Items\n" +
                        "· Large Ember\n" +
                        "· Ring of the Evil Eye\n" +
                        "· Spider Shield\n" +
                        "· Humanity\n" +
                        "· Soul of a Nameless Soldier\n" +
                        "· Large Soul of a Nameless Soldier\n" +
                        "· Sewer Chamber Key\n" +
                        "· Greataxe\n" +
                        "· Warrior Set\n" +
                        "· Sunlight Medal\n" +
                        "· Heavy Crossbow\n" +
                        "· Heavy Bolts\n" +
                        "· Blighttown Key",

                "zonesdepths",""));

        dataModels.add(new DataModel("Blighttown",
                "NPCS in the area\n" +
                        "· Maneater Mildred\n" +
                        "· Siegmeyer of Catarina\n" +
                        "· Quelana of Izalith\n" +
                        "· Shiva of the East\n\n" ,

                        "Bosses: \n" +
                        "· Chaos Witch Quelaag\n\n" ,

                        "Enemies\n" +
                        "· Infested Barbarian\n" +
                        "· Infested Ghoul\n" +
                        "· Flaming Attack Dog\n" +
                        "· Giant Mosquito\n" +
                        "· Cragspider\n" +
                        "· Giant Leech" +
                        "· Egg Carrier\n" +
                        "· Blowdart Sniper\n" +
                        "· Parasitic Wall Hugger",

                "Items\n" +
                        "· Soul of a Proud Knight\n" +
                        "· Blooming Purple Moss Clump\n" +
                        "· Purple Moss Clump\n" +
                        "· Humanity\n" +
                        "· Twin Humanities\n" +
                        "· Shadow Set\n" +
                        "· Iaito\n" +
                        "· Eagle Shield\n" +
                        "· Pyromancy: Power Within\n" +
                        "· Large Soul of a Nameless Soldier\n" +
                        "· Whip\n" +
                        "· Wanderer Set\n" +
                        "· Falchion\n" +
                        "· Dragon Scale\n" +
                        "· Butcher Knife\n" +
                        "· Pyromancy: Poison Mist\n" +
                        "· Pyromancer Set\n" +
                        "· Large Titanite Shard\n" +
                        "· Server\n" +
                        "· Plank Shield\n" +
                        "· Green Titanite Shard\n" +
                        "· Great Club\n" +
                        "· Fire Keeper Soul\n" +
                        "· Crimson Set\n" +
                        "· Tin Banishment Catalyst\n" +
                        "· Sorcery: Remedy\n" +
                        "· Key to New Londo Ruins",

                "zonesblighttown",""));

        dataModels.add(new DataModel("Quelaag's Domain",
                "NPCS in the area\n" +
                        "· Quelaag's Sister\n" +
                        "· Eingyi\n\n" ,

                        "Bosses: \n" +
                        "· Chaos Witch Quelaag\n\n" ,

                        "Enemies\n" +
                        "· Egg-Burdened",

                "Items\n" +
                        "· Egg Vermifuge\n" +
                        "· Soul of Quelaag",

                "zonesquelaagsdomain",""));

        dataModels.add(new DataModel("The Great Hollow",
                "NPCS in the area\n" +
                        "· -\n\n" ,

                        "Bosses: \n" +
                        "· -\n\n" ,

                        "Enemies\n" +
                        "· Basilisk\n" +
                        "· Crystal Lizard\n" +
                        "· Mushroom People",

                "Items\n" +
                        "· Cloranthy Ring\n" +
                        "· Titanite Chunk\n" +
                        "· Red Titanite Chunk\n" +
                        "· Blue Titanite Chunk\n" +
                        "· White Titanite Chunk\n" +
                        "· Large Soul of a Nameless Soldier",

                "zonesgreathollow",""));

        dataModels.add(new DataModel("Ash Lake",
                "NPCS in the area\n" +
                        "· Everlasting Dragon\n\n" ,

                        "Bosses: \n" +
                        "· Black Hydra\n\n" ,

                        "Enemies\n" +
                        "· Man-Eater Shell\n" +
                        "· Basilisk\n" +
                        "· Mushroom Parent",

                "Items\n" +
                        "· Dragon Scale\n" +
                        "· Miracle: Great Magic Barrier\n" +
                        "· Dragon Greatsword",

                "zonesashlake",""));

        dataModels.add(new DataModel("Sen's Fortress",
                "NPCS in the area\n" +
                        "· Big Hat Logan\n" +
                        "· Creastfallen Merchant\n" +
                        "· Siegmeyer of Catarina\n" +
                        "· Iron Tarkus\n\n" ,

                        "Bosses: \n" +
                        "· Iron Golem\n" +
                        "· Titanite Demon\n" +
                        "· Firebomb Giant\n" +
                        "· Gate Giant\n" +
                        "· Rock Giant\n" +
                        "· Tower Knight\n" +
                        "· Ricard the Archer\n\n" ,

                        "Enemies\n" +
                        "· Serpent Soldier\n" +
                        "· Serpent Mages\n" +
                        "· Undead Knight of Balder\n" +
                        "· Mimic",

                "Items\n" +
                        "· Cage Key\n" +
                        "· Shotel\n" +
                        "· Rare Ring of Sacrifice\n" +
                        "· Sniper Crossbow\n" +
                        "· Covetous Gold Serpent Ring\n" +
                        "· Slumbering Dragoncrest Ring\n" +
                        "· Ring Of Steel Protection\n" +
                        "· Black Sorcerer set",

                "zonessensfortress",""));

        dataModels.add(new DataModel("Anor Londo",
                "NPCS in the area\n" +
                        "· Darkmoon Knightess\n" +
                        "· Giant Blacksmith\n" +
                        "· Gwynevere\n" +
                        "· Gwyndolin\n" +
                        "· Siegmeyer of Catarina\n" +
                        "· Solaire of Astora\n\n" ,

                        "Bosses: \n" +
                        "· Ornstein and Smough\n" +
                        "· Dark Sun Gwyndolin\n\n" ,

                        "Enemies\n" +
                        "· Sentinel\n" +
                        "· Mmimic\n" +
                        "· Gargoyle\n" +
                        "· Painting Guardian\n" +
                        "· Bat Wing Demon\n" +
                        "· Silver Knight\n" +
                        "· Royal Sentinel\n" +
                        "· Titanite Demon",

                "Items\n" +
                        "· Crystal Halberd\n" +
                        "· Occult Club\n" +
                        "· Havel's Armor Set\n" +
                        "· Black Iron Armor Set\n" +
                        "· Soul of a Hero\n" +
                        "· Havel's Greatshield\n" +
                        "· Dragon Tooth\n" +
                        "· Silver Coin\n" +
                        "· Gold Coin\n" +
                        "· Sunlight Medal\n" +
                        "· Silver Knight Armor Set\n" +
                        "· Dragon Slayer Bow\n" +
                        "· Demon Titanite\n" +
                        "· Divine Blessing\n" +
                        "· Hawk Ring\n" +
                        "· Tiny Being's Ring",

                "zonesanorlondo",""));

        dataModels.add(new DataModel("Painted World of Ariamis",
                "NPCS in the area\n" +
                        "· Crossbreed Priscilla\n\n" ,

                        "Bosses: \n" +
                        "· King Jeremiah\n" +
                        "· Undead Dragon\n" +
                        "· Crossbreed Priscilla\n\n" ,

                        "Enemies\n" +
                        "· Hollow\n" +
                        "· Engorged Zombie\n" +
                        "· Snow Rat\n" +
                        "· Crow Demon\n" +
                        "· Wheel Skeleton\n" +
                        "· Phalanx\n" +
                        "· Tower Knight",

                "Items\n" +
                        "· Black Set\n" +
                        "· Velka's Rapier\n" +
                        "· Painting Guardian Set\n" +
                        "· Bloodshield\n" +
                        "· Red Sign Soapstone\n" +
                        "· Dark Ember\n" +
                        "· Pyromancy: Fire Surge\n" +
                        "· Pyromancy: Acid Surge\n" +
                        "· Miracle: Vow of Silence\n" +
                        "· Egg Vermifuge\n" +
                        "· Humanity\n" +
                        "· Ring of Sacrifice\n" +
                        "· Dried Finder\n" +
                        "· Gold Coin",

                "zonespaintedworldofariamis",""));

        dataModels.add(new DataModel("Darkroot Garden",
                "NPCS in the area\n" +
                        "· Alvina of the Darkroot Wood\n" +
                        " · Shiva of the East\n\n" ,

                        "Bosses: \n" +
                        "· Giant Cats\n" +
                        "· Moonlight Butterfly\n" +
                        "· Great Grey Wolf Sif\n\n" ,

                        "Enemies\n" +
                        "· Ent\n" +
                        "· Great Stone Knight\n" +
                        "· Frog-Ray\n" +
                        "· Tree Lizard\n" +
                        "· Mushroom People\n" +
                        "· Forest Dwellers",

                "Items\n" +
                        "· Partizan\n" +
                        "· Wolf Ring\n" +
                        "· Enchanted Ember\n" +
                        "· Elite Knight Armor Set\n" +
                        "· Gold Pine Resin\n" +
                        "· Eastern Set\n" +
                        "· Hornet Ring\n" +
                        "· Purple Moss Clump\n" +
                        "· Blooming Purple Moss Clump\n" +
                        "· Bloodred Moss Clump\n" +
                        "· Egg Vermifuge\n" +
                        "· Divine Ember\n" +
                        "· Stone Knight Set\n" +
                        "· Dark Wood Grain Ring",

                "zonesdarkrootgarden",""));

        dataModels.add(new DataModel("Darkroot Basin",
                "NPCS in the area\n" +
                        "· Dusk of Oolacile\n" +
                        " · Havel the Rock\n\n" ,

                        "Bosses: \n" +
                        "· Hydra\n" +
                        "· Golden Crystal Golem\n" +
                        "· Black Knight\n\n" ,

                        "Enemies\n" +
                        "· Crystal Golem\n" +
                        "· Crystal Lizard",

                "Items\n" +
                        "· Hunter Set\n" +
                        "· Grass Crest Shield\n" +
                        "· Dusk Crown Ring\n" +
                        "· Antiquated Set\n" +
                        "· Longbow\n" +
                        "· Knight Set\n" +
                        "· Pharis's Hat\n" +
                        "· Black Bow of Pharis",

                "zonesdarkrootbasin",""));

        dataModels.add(new DataModel("New Londo Ruins",
                "NPCS in the area\n" +
                        "· Ingward\n" +
                        "· Rickert of Vinheim\n" +
                        "· Witch Beatrice\n\n" ,

                        "Bosses: \n" +
                        "· Four Kings\n\n" ,

                        "Enemies\n" +
                        "· Hollow\n" +
                        "· Crestfallen Warrior\n" +
                        "· Ghost\n" +
                        "· Banshee\n" +
                        "· Darkwraith Knight\n" +
                        "· Mass of Souls\n" +
                        "· Wisp",

                "Items\n" +
                        "· Cursebite Ring\n" +
                        "· Transient Curse\n" +
                        "· Fire Keeper Soul\n" +
                        "· Rare Ring of Sacrifice",

                "zonesnewlondoruins",""));

        dataModels.add(new DataModel("The Duke's Archives",
                "NPCS in the area\n" +
                        "· Big Hat Logan\n" +
                        "· Sieglinde of Catarina\n\n" ,

                        "Bosses: \n" +
                        "· Fang Boar\n" +
                        "· Golden Crystal Golem\n" +
                        "· Seath the Scaleless\n\n" ,

                        "Enemies\n" +
                        "· Fang Boar\n" +
                        "· Undead Crystal Soldiers\n" +
                        "· Channelers\n" +
                        "· Mimic\n" +
                        "· Serpent Soldiers\n" +
                        "· Serpent Mage\n" +
                        "· Pisacas\n" +
                        "· Crystal Golems\n" +
                        "· Crystal Knight",

                "Items\n" +
                        "· Crystal Knight Shield\n" +
                        "· Maiden Armour Set\n" +
                        "· White Seance Ring\n" +
                        "· Fire Keeper Soul\n" +
                        "· Avelyn\n" +
                        "· Sorcery: Strong Magic Shield\n" +
                        "· Prism Stones\n" +
                        "· Crystal Ember\n" +
                        "· Blue Titanite Chunk\n" +
                        "· Twinkling Titanite\n" +
                        "· Miracle: Soothing Sunlight\n" +
                        "· Miracle: Boundtiful Sunlight\n" +
                        "· Enchanted Falchion\n" +
                        "· Channeler Set\n" +
                        "· Crystalline Set\n" +
                        "· Soul of a Great Hero\n" +
                        "· Large Magic Ember\n" +
                        "· Big Hat's Set\n" +
                        "· Tin Crystallitzation Catalyst\n" +
                        "· Logan's Catalyst\n" +
                        "· Sorcery: White Dragon Breath",

                "zonesdukesarchives",""));

        dataModels.add(new DataModel("Crystal Cave",
                "NPCS in the area\n" +
                        "· -\n\n" ,

                        "Bosses: \n" +
                        "· Golden Crystal Golem\n" +
                        "· Seath the Scaleless\n\n" ,

                        "Enemies\n" +
                        "· Crystal Golems\n" +
                        "· Moonlight Butterfly\n" +
                        "· Man-Eater Shell",

                "Items\n" +
                        "· Blue Titanite Slab\n" +
                        "· Blue Titanite Chunk\n" +
                        "· Humanity\n" +
                        "· Soul of a Hero\n" +
                        "· Moonlight Greatsword",

                "zonescrystalcave",""));

        dataModels.add(new DataModel("Demon Ruins",
                "NPCS in the area\n" +
                        "· Kirk, The Knight of Thorns\n\n" ,

                        "Bosses: \n" +
                        "· Ceaseless Discharge\n" +
                        "· Demon Firesage\n" +
                        "· Centipede Demon\n\n" ,

                        "Enemies\n" +
                        "· Eingyi\n" +
                        "· Taurus Demon\n" +
                        "· Captra Demon\n" +
                        "· Stone Demon\n" +
                        "· Burrowing Rockworm",

                "Items\n" +
                        "· Large Flame Ember\n" +
                        "· Chaos Flame Ember\n" +
                        "· Soul of a Brave Warrior\n" +
                        "· Soul of a Proud Knight\n" +
                        "· Large Soul of a Proud Knight\n" +
                        "· Gold-Hemmed Black Armor Set\n" +
                        "· Green Titanite Shard\n" +
                        "· Orange Charred Ring",

                "zonesdemonruins",""));

        dataModels.add(new DataModel("Lost Izalith",
                "NPCS in the area\n" +
                        "· Solaire of Astora\n" +
                        "· Kirk, The Knight of Thorns\n" +
                        "· Siegmeyer of Catarina\n" +
                        "· Daughter of Chaos\n\n" ,

                        "Bosses: \n" +
                        "· Chaos Witch Quelaag\n\n" ,

                        "Enemies\n" +
                        "· Bounding Demon of Izalith\n" +
                        "· Stone Demon\n" +
                        "· Chaos Eater\n" +
                        "· Titanite Demon\n" +
                        "· Sunlight Maggot",

                "Items\n" +
                        "· Twin Humanities\n" +
                        "· Divine Blessing\n" +
                        "· Chaos Fire Whip\n" +
                        "· Ring of Sacrifice\n" +
                        "· Soul of a Great Hero\n" +
                        "· Red Titanite Slab",

                "zoneslostizalith",""));

        dataModels.add(new DataModel("The Catacombs",
                "NPCS in the area\n" +
                        "· Blacksmith Vamos\n" +
                        "· Patches the Hyena\n" +
                        "· Gravelord Nito\n\n" ,

                        "Bosses: \n" +
                        "· Pinwheel\n\n" ,

                        "Enemies\n" +
                        "· Skeleton\n" +
                        "· Undead Mage\n" +
                        "· Wisp\n" +
                        "· Titanite Demon\n" +
                        "· Black Knight\n" +
                        "· Wheel Skeleton\n" +
                        "· Giant Skeleton\n" +
                        "· Crystal Lizard",

                "Items\n" +
                        "· Darkmoon Seance Ring\n" +
                        "· Tranquil Walk of Peace\n" +
                        "· Eye of Death\n" +
                        "· Mace\n" +
                        "· Great Scythe\n" +
                        "· Lucerne\n" +
                        "· Cleric Set",

                "zonescatacombs",""));

        dataModels.add(new DataModel("Tomb of the Giants",
                "NPCS in the area\n" +
                        "· Patches the Hyena\n" +
                        "· Reah of Thorolund\n\n",

                        "Bosses: \n" +
                        "· Nito\n\n" ,

                        "Enemies\n" +
                        "· Giant Skeleton\n" +
                        "· Giant Skeleton Doc\n" +
                        "· Bone Tower\n" +
                        "· Baby Skeleton\n" +
                        "· Pinwheel\n" +
                        "· Hollowed Clerics\n" +
                        "· Leeroy",

                "Items\n" +
                        "· Skull Lantern\n" +
                        "· Large Divine Ember\n" +
                        "· Covetous Silver Serpent Ring\n" +
                        "· Effigy Shield\n" +
                        "· White Titanite Slab",

                "zonestombsofgiants",""));

        dataModels.add(new DataModel("Kiln of the First Flame",
                "NPCS in the area\n" +
                        "· -\n\n" ,

                        "Bosses: \n" +
                        "· Gwyn, Lord of Cinder\n\n" ,

                        "Enemies\n" +
                        "· Black Knights",

                "Items\n" +
                        "· Black Knight Armor Set",

                "zoneskilnoffirstflame",""));

        adapter = new CustomAdapter(dataModels, getApplicationContext());
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                DataModel dataModel = dataModels.get(position);

                Intent i = new Intent(ZoneList.this, ZoneDescription.class);
                i.putExtra("name", dataModel.getName());
                i.putExtra("npc", dataModel.getZone_npc());
                i.putExtra("boss", dataModel.getZone_boss());
                i.putExtra("enemy", dataModel.getZone_enemy());
                i.putExtra("items", dataModel.getItems());
                i.putExtra("imgsrc", dataModel.getImagesrc());

                startActivity(i);
            }
        });
    }
}
