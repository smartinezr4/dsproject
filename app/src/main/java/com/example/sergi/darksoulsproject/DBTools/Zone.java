package com.example.sergi.darksoulsproject.DBTools;

/**
 * Created by sergi on 20-Feb-18.
 */

public class Zone {

    private int _id;
    String _zone_boss, _zone_enemy, _zone_npc, _items, _imagesrc;

    //Empty constructor
    public Zone() {

    }

    //constructor
    public Zone(int id, String zone_boss, String zone_enemy, String zone_npc, String items, String imagesrc) {
        this._id = id;
        this._zone_boss = zone_boss;
        this._zone_enemy = zone_enemy;
        this._zone_npc = zone_npc;
        this._items = items;
        this._imagesrc = imagesrc;
    }

    public int get_id() {
        return _id;
    }

    public void set_id(int _id) {
        this._id = _id;
    }

    public String get_zone_boss() {
        return _zone_boss;
    }

    public void set_zone_boss(String _zone_boss) {
        this._zone_boss = _zone_boss;
    }

    public String get_zone_enemy() {
        return _zone_enemy;
    }

    public void set_zone_enemy(String _zone_enemy) {
        this._zone_enemy = _zone_enemy;
    }

    public String get_zone_npc() {
        return _zone_npc;
    }

    public void set_zone_npc(String _zone_npc) {
        this._zone_npc = _zone_npc;
    }

    public String get_items() {
        return _items;
    }

    public void set_items(String _items) {
        this._items = _items;
    }

    public String get_imagesrc() {
        return _imagesrc;
    }

    public void set_imagesrc(String _imagesrc) {
        this._imagesrc = _imagesrc;
    }
}
