package com.example.sergi.darksoulsproject;

import android.support.v7.app.AppCompatActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.net.Uri;
import android.provider.MediaStore;
import android.widget.Toast;

import com.example.sergi.darksoulsproject.DBTools.Boss;
import com.example.sergi.darksoulsproject.DBTools.DatabaseHandler;

/**
 * Created by Garnau on 21/02/2018.
 */

public class AddBossActivity extends AppCompatActivity {

    ImageView imageView;
    Button button, btnSave, buttonClean;
    private static final int PICK_IMAGE = 100;
    Uri imageUri;

    private static EditText edtName;
    private static EditText edtLocation;
    private static EditText edtDiff;
    private static EditText edtDescrip;
    private static EditText edtDrop;
    private static ImageView bossImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.addboss_activity);

        imageView = (ImageView)findViewById(R.id.addBossImage);
        button = (Button)findViewById(R.id.addImage);
        btnSave = findViewById(R.id.saveBoss);
        buttonClean = (Button)findViewById(R.id.cleanBoss);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v){
                OpenGallery();
            }
        });
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v){
                AddBoss();
            }
        });

        buttonClean.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) { CleanBoss();}
        });
    }

    private void CleanBoss() {

        ObtenirCamps();

        edtName.setText("");
        edtLocation.setText("");
        edtDiff.setText("");
        edtDescrip.setText("");
        edtDrop.setText("");
        bossImage.setImageBitmap(null);
    }

    private void ObtenirCamps() {
        edtName = (EditText)findViewById(R.id.bossName);
        String bossName = edtName.getText().toString();

        edtLocation = (EditText)findViewById(R.id.bossLocation);
        String bossLocation = edtLocation.getText().toString();

        edtDiff = (EditText)findViewById(R.id.bossDifficulty);
        String bossDiff = edtDiff.getText().toString();

        edtDescrip = (EditText)findViewById(R.id.bossDescription);
        String bossDescrip = edtDescrip.getText().toString();

        edtDrop = (EditText)findViewById(R.id.bossDrops);
        String bossDrop = edtDrop.getText().toString();

        bossImage = findViewById(R.id.addBossImage);
    }

    private void OpenGallery(){
        Intent gallery = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.INTERNAL_CONTENT_URI);
        startActivityForResult(gallery, PICK_IMAGE);
    }
    private void AddBoss(){
        EditText edtName = (EditText)findViewById(R.id.bossName);
        String bossName = edtName.getText().toString();
        EditText edtLocation = (EditText)findViewById(R.id.bossLocation);
        String bossLocation = edtLocation.getText().toString();
        EditText edtDiff = (EditText)findViewById(R.id.bossDifficulty);
        String bossDiff = edtDiff.getText().toString();
        EditText edtDrop = (EditText)findViewById(R.id.bossDrops);
        String bossDrop = edtDrop.getText().toString();
        EditText edtDescript = findViewById(R.id.bossDescription);
        String bossDescript = edtDescript.getText().toString();
        //ImageView bossImage = findViewById(R.id.addBossImage);

        //DB Stuff
        DatabaseHandler db = new DatabaseHandler(this);
        db.addBoss(new Boss(bossName, bossLocation, bossDiff, bossDrop, bossDescript,"not_implemented"));
        Toast.makeText(getApplicationContext(), "Boss added succesfully", Toast.LENGTH_LONG).show();
        CleanBoss();


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode == RESULT_OK && requestCode == PICK_IMAGE) {
            imageUri = data.getData();
            imageView.setImageURI(imageUri);
        }
    }

}
