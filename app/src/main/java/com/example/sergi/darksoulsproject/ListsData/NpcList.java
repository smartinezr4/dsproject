package com.example.sergi.darksoulsproject.ListsData;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import com.example.sergi.darksoulsproject.CustomAdapter;
import com.example.sergi.darksoulsproject.DataModel;
import com.example.sergi.darksoulsproject.R;

import java.util.ArrayList;

/**
 * Created by Scud on 09/02/2018.
 */

public class NpcList extends AppCompatActivity {
    ArrayList<DataModel> dataModels;
    ListView listView;
    public static CustomAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.content_main2);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        listView=findViewById(R.id.list);


        dataModels= new ArrayList<>();

        String str2 = "Location: ";

        dataModels.add(new DataModel("Blacksmith Andre", str2+"Undead Parish", "","Drops: 1,000 Souls, 3 Humanities, Blacksmith Hammer, Crest of Artorias", "Andre of Astora is a Character in Dark Souls. He is located in the basement of the old church, a disused building that connects the Undead Parish with the Darkroot Garden. He will repair and upgrade your weapons and armor for a fee of souls and materials.", "andre"));
        dataModels.add(new DataModel("Crestfallen Warrior", str2+"Firelink Shrine", "","Drops: 1,000 souls", "He is the first NPC you will meet at Firelink Shrine. He has grown weary of the world and fears his hollowing is inevitable. When engaging in conversation with him, he will reveal useful information about the world and your objectives.", "crestfallen"));
        dataModels.add(new DataModel("Anastacia of Astora", str2+"Firelink Shrine", "","Drops: Cannot be killed by the player.", "Anastacia of Astora, also known as the Ash Maiden, is the fire keeper of Firelink Shrine.", "anastacia"));
        dataModels.add(new DataModel("Solaire of Astora", str2+"Undead Parish (first encounter)", "","Drops: 1,500 souls, 2 Humanity, Sunlight Straight Sword, Sunlight Talisman, Sunlight Shield, Iron and Sun Set", "He is an exceptionally skilled warrior who purposefully became Undead so he could visit Lordran in his quest to find the sun.", "solaire"));
        dataModels.add(new DataModel("Knight Lautrec of Carim", str2+"Undead Parish", "","Drops: 5 Humanity, Ring of Favor and Protection, Fire Keeper Soul", "A knight from Carim and a devout of the goddess Fina. His love for Fina makes him a psychopath who doesn't care about other people and readily kills them after they served their purposes.", "lautrec"));
        dataModels.add(new DataModel("Oswald of Carim", str2+"Undead Parish", "","Drops: 2,000 souls, Book of the Guilty, 1-2 Twin Humanities", "He is a grim character who deals in sinners and sin. He will appear at the base of the bell-tower, in Undead Parish, after the Gargoyles are defeated and the bell is rung. ", "Oswald"));
        dataModels.add(new DataModel("Quelaan", str2+"Quelaag's Domain", "","Drops: Fire Keeper Soul", "Quelaan is one of the seven Daughters of Chaos, and one of the more unfortunate ones who was mutated into a half-spider entity by the Bed of Chaos. She is rather more docile than her sister Quelaag however and is both the leader of the Chaos Servant Covenant and the fire keeper of the bonfire she sits beside.", "quelaan"));
        dataModels.add(new DataModel("Quelana", str2+"Blighttown", "","Drops: 1,000 souls, Fire Tempest", "She is one of seven daughters sired by the Witch of Izalith. She's known to be the mother of pyromancy, as she's the one who taught the great pyromancer Salaman. She escaped/ran away before she could be transformed into a demon due to Bed of Chaos. Unfortunately, her sisters did not escape in time.", "quelana"));
        dataModels.add(new DataModel("Eingyi of the Great Swamp", str2+"Quelaag's Domain", "","Drops: 2 Egg Vermifuge", "Once a pyromancer from the great swamp, Eingyi was considered a heretic, even by his bretheren, because of his toxic pyromancy. He was driven from the great swamp and got infected by the blightpuss. The white Spider Witch took him in and swallowed the blightpuss, despite Quelaag's orders to the contrary, and he has been her loyal servant ever since, although still burdened as an egg carrier.", "eingyi"));
        dataModels.add(new DataModel("Siegmeyer of Catarina", str2+"Sen's Fortress (first encounter)", "","Drops: 1,000 souls, Speckled Stoneplate Ring, 3 Humanity", "A knight swearing allegiance to an unidentified order of warriors, this NPC can occasionally assist the player in various points in the game. It is possible for this older gentleman knight to die in the course of aiding you, so be careful.", "siegmeyer"));
        dataModels.add(new DataModel("Sieglinde of Catarina", str2+"Crystal Caves", "","Drops: 1,000 souls, 10 Humanity can be absorbed from Sieglinde", "Sieglinde of Catarina is a Character in Dark Souls. She is the daughter of Siegmeyer of Catarina. She is first found in the yard between the Duke's Archives and the Crystal Cave, and is essential to Siegmeier's storyline.", "catarina"));
        dataModels.add(new DataModel("Ingward", str2+"New Londo Ruins", "","Drops: 1,000 souls, Key to the Seal", "He is the last remaining of the three New Londo sorcerers who long ago undertook the task of guarding the seal; to pay atonement for all who were sacrificed when they sealed away the darkness. Two of the three former healers have since forsaken New Londo, after tiring of their duties.", "ingward"));
        dataModels.add(new DataModel("Kingseeker Frampt", str2+"Firelink Shrine", "","Drops: Nothing, but he does not die. He will retreat into his hole and will not return.", "Kingseeker Frampt, the Primordial Serpent, is in deep slumber in Firelink Shrine. He awakes after the second bell of awakening has been rung, and can be found inside the watery area of the church ruins. His snoring can be heard in the area, after the first bell-chime.", "frampt"));
        dataModels.add(new DataModel("Darkstalker Kaathe", str2+"The Abyss", "","Drops: Cracked Red Eye Orb", "He is a Primordial Serpent, just like Kingseeker Frampt. He is the Leader of the Darkwraiths Covenant, and he who tempted The Four Kings with the power of Life Drain which led to the eventual flooding of New Londo.", "kaathe"));
        dataModels.add(new DataModel("Gwynevere", str2+"Anor Londo", "","Drops: Lordvessel (if not already given to the player)", "She is the Princess of Sunlight and daughter of Lord Gwyn. She gives the Lordvessel to the player and allows him to join the Princess' Guard Covenant. The Gwynevere you see in game is an illusion created by Gwyndolin  in order to manipulate the player.", "gwynevere"));
        dataModels.add(new DataModel("'Trusty' Patches", str2+"The Catacombs, Tomb of Giants and Firelink Shrine", "","Drops: 2,000 souls, 4 Humanity, Crescent Axe", "Patches returns with his deceiving ways to trick you into losing your life and treasures, but will eventually sell you some good cleric gear and other useful items. He is quite notable for having a strong distaste for clerics.", "patches"));
        dataModels.add(new DataModel("Petrus of Thorolund", str2+"Firelink Shrine", "","Drops: 1,000 souls, 2 Humanity", "Petrus is a blond Cleric who is waiting for Lady Reah of Thorolund and her two bodyguards at Firelink Shrine.", "petrus"));
        dataModels.add(new DataModel("Reah of Thorolund", str2+"Firelink Shrine", "","Drops: 7 Humanity, Ivory Talisman", "Reah of Thorolund, the youngest daughter of the House of Thorolund, is a maiden on a mission, and will appear after defeating the Capra Demon.", "reah"));
        dataModels.add(new DataModel("Dusk of Oolacile", str2+"Darkroot Basin", "","Drops: 1,000 souls", "Dusk was the princess of Oolacile at the time it was attacked by the abyss. She was saved by a knight she believes to be Artorias and thus survived the destruction of Oolacile, becoming the only known survivor.", "dusk"));
        dataModels.add(new DataModel("Big Hat Logan", str2+"Sen's Fortress (first encounter)", "","Drops: 1,000 souls, Big Hat", "Once a royal member of Dragon School, he is now a scholar of the soul arts. He turned Undead over a hundred years ago, and has been roaming Lordran, searching for more wisdom, ever since.", "logan"));
        dataModels.add(new DataModel("Griggs of Vinheim", str2+"Undead Burg (lower)", "","Drops: 1,000 souls, 1 Humanity, Sorcery: Hush, Slumbering Dragoncrest Ring", "A sorcerer from Vinheim Dragon School. He came to Lordran in search of his master, Big Hat Logan, who has departed from Vinheim in his search for knowledge.", "griggs"));
        dataModels.add(new DataModel("Domhnall of Zena", str2+"Depths, before the boss room", "","Drops: 1,000 souls", "Domhnall is an adventurer from historical Zena. He sells unique items, armor and crystal weapons.", "domhnall"));
        dataModels.add(new DataModel("Laurentius of the Great Swamp", str2+"The Depths (first encounter)", "","Drops: 1,000 souls", "He is a Pyromancy trainer rescued from The Depths. Was apparently being held captive by an undead chef.", "laurentius"));
        //dataModels.add(new DataModel("", str2+"", "","Drops: ", "", ""));

        adapter= new CustomAdapter(dataModels,getApplicationContext());
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                DataModel dataModel= dataModels.get(position);

                Intent i = new Intent(NpcList.this, NpcDescription.class);
                i.putExtra("name", dataModel.getName());
                i.putExtra("location", dataModel.getZone());
                i.putExtra("drop", dataModel.getDrops());
                i.putExtra("extrainfo", dataModel.getExtrainfo());
                i.putExtra("imgsrc", dataModel.getImagesrc());

                startActivity(i);
            }
        });
    }
}
