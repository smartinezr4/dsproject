package com.example.sergi.darksoulsproject.DBTools;

import android.widget.ImageView;

/**
 * Created by sergi on 20-Feb-18.
 */

public class Boss {

    private int _id;
    private String _name, _zone, _difficulty, _drops, _extrainfo, _imagesrc;
    private ImageView _imgView;
    //Empty constructor
    public Boss() {

    }
    public Boss(String bossName, String bossLocation, String bossDiff, String bossDrop, String s){}

    //No ID constructor
    public Boss(String name, String zone, String difficulty, String drops, String extrainfo, String imagesrc) {
        this._name = name;
        this._zone = zone;
        this._difficulty = difficulty;
        this._drops = drops;
        this._extrainfo = extrainfo;
        this._imagesrc = imagesrc;
    }

    //Full Constructor
    public Boss(int id, String name, String zone, String difficulty, String drops, String extrainfo, String imagesrc) {
        this._id = id;
        this._name = name;
        this._zone = zone;
        this._difficulty = difficulty;
        this._drops = drops;
        this._extrainfo = extrainfo;
        this._imagesrc = imagesrc;
    }
    //Full Constructor amb imatge
//    public Boss(int id, String name, String zone, String difficulty, String drops, ImageView imageView) {
//        this._id = id;
//        this._name = name;
//        this._zone = zone;
//        this._difficulty = difficulty;
//        this._drops = drops;
//        this._imgView = imageView;
//    }

    public int get_id() {
        return _id;
    }

    public void set_id(int _id) {
        this._id = _id;
    }

    public String get_name() {
        return _name;
    }

    public void set_name(String _name) {
        this._name = _name;
    }

    public String get_zone() {
        return _zone;
    }

    public void set_zone(String _zone) {
        this._zone = _zone;
    }

    public String get_difficulty() {
        return _difficulty;
    }

    public void set_difficulty(String _difficulty) {
        this._difficulty = _difficulty;
    }

    public String get_drops() {
        return _drops;
    }

    public void set_drops(String _drops) {
        this._drops = _drops;
    }

    public String get_extrainfo() {
        return _extrainfo;
    }

    public void set_extrainfo(String _extrainfo) {
        this._extrainfo = _extrainfo;
    }

    public String get_imagesrc() {
        return _imagesrc;
    }

    public void set_imagesrc(String _imagesrc) {
        this._imagesrc = _imagesrc;
    }
}
