package com.example.sergi.darksoulsproject.DBTools;

/**
 * Created by sergi on 20-Feb-18.
 */

public class NPC {

    private int _id;
    private String _name, _zone, _drops, _extrainfo, _imagesrc;

    public NPC() {

    }

    public NPC(int id, String name, String zone, String drops, String extrainfo, String imagesrc) {
        this._id = id;
        this._name = name;
        this._drops = drops;
        this._extrainfo = extrainfo;
        this._imagesrc = imagesrc;
    }

    public int get_id() {
        return _id;
    }

    public void set_id(int _id) {
        this._id = _id;
    }

    public String get_name() {
        return _name;
    }

    public void set_name(String _name) {
        this._name = _name;
    }

    public String get_zone() {
        return _zone;
    }

    public void set_zone(String _zone) {
        this._zone = _zone;
    }

    public String get_drops() {
        return _drops;
    }

    public void set_drops(String _drops) {
        this._drops = _drops;
    }

    public String get_extrainfo() {
        return _extrainfo;
    }

    public void set_extrainfo(String _extrainfo) {
        this._extrainfo = _extrainfo;
    }

    public String get_imagesrc() {
        return _imagesrc;
    }

    public void set_imagesrc(String _imagesrc) {
        this._imagesrc = _imagesrc;
    }
}
