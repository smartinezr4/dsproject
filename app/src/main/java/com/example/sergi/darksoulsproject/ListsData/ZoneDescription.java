package com.example.sergi.darksoulsproject.ListsData;

import android.content.res.Resources;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.sergi.darksoulsproject.R;

/**
 * Created by Scud on 13/02/2018.
 */

public class ZoneDescription extends AppCompatActivity {
    TextView txtName, txtNpc, txtBoss, txtEnemy, txtItems;
    ImageView imgView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.description_activity_2);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        String name = getIntent().getStringExtra("name");
        String npc = getIntent().getStringExtra("npc");
        String boss = getIntent().getStringExtra("boss");
        String enemy = getIntent().getStringExtra("enemy");
        String items = getIntent().getStringExtra("items");
        String imgsrc = getIntent().getStringExtra("imgsrc");

        txtName = findViewById(R.id.txttitol);
        txtName.setText(name);

        txtNpc = findViewById(R.id.txtnpc);
        txtNpc.setText(npc);
        txtNpc = findViewById(R.id.txtboss);
        txtNpc.setText(boss);
        txtNpc = findViewById(R.id.txtenemy);
        txtNpc.setText(enemy);

        txtItems = findViewById(R.id.txtitems);
        txtItems.setText(items);

        imgView = findViewById(R.id.imgsrc);
        Resources res = getResources();
        imgView.setImageResource(res.getIdentifier(imgsrc,"drawable",getPackageName()));

    }
}
