package com.example.sergi.darksoulsproject.ListsData;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import com.example.sergi.darksoulsproject.CustomAdapter;
import com.example.sergi.darksoulsproject.DBTools.Boss;
import com.example.sergi.darksoulsproject.DBTools.DatabaseHandler;
import com.example.sergi.darksoulsproject.DataModel;
import com.example.sergi.darksoulsproject.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Scud on 09/02/2018.
 */

public class BossList extends AppCompatActivity {
    ArrayList<DataModel> dataModels;
    ListView listView;
    public static CustomAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.content_main2);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        listView = findViewById(R.id.list);

        dataModels = new ArrayList<>();

        String str2 = "Location: ";
        String str3 = "Difficulty: ";
        dataModels.add(new DataModel("Asylum Demon", str2 + "Undead Asylum", str3 + "Easy", "Drops: 2,000 souls, 1 Humanity, Big Pilgrim's Key, Demon's Great Hammer", "Large demon found during the tutorial in the Undead Asylum. This boss is very similar to the vanguard in Demon's Souls in both attack style and appearance in the story. After escaping from your cell and lighting the first bonfire, you will open a large set of doors leading to another courtyard surrounded by large vases.", "asylumdemon"));
        dataModels.add(new DataModel("Taurus Demon", str2 + "Undead Burg", str3 + "Easy", "Drops: 3,000 souls, 1 Humanity, 1 Homeward Bone, Demon's Greataxe (rare)", "A large demon found on top of a narrow castle wall in the Undead Burg, on the road to the Undead Parish. Similar to the Bell Gargoyles encountered later, the Taurus Demon does not immediately appear when the host passes through the fog gate.", "taurus"));
        dataModels.add(new DataModel("Bell Gargoyle", str2 + "Undead Burg", str3 + "Medium", "Drops: 10,000 souls, 1 Twin Humanity, Gargoyle Tail Axe (cut tail), Gargoyle's Halberd (rare), Gargoyle Shield (rare), Gargoyle Helm (rare)", "Located on the roof of the Undead Parish, guarding the path to one of the Bells of Awakening. Not just one, but two of these axe-wielding, fire breathing demons must be killed for the player to proceed.", "gargoyle"));
        dataModels.add(new DataModel("Capra Demon", str2 + "Undead Burg", str3 + "Easy", "Drops: 6,000 souls, 1 Humanity, 1 Homeward Bone, Demon Great Machete (rare)", "The Capra Demon is the boss of the lower section of the Undead Burg. You can get to him either by going left down to the end of the main street (past the Undead Thieves and Poison Dogs), or by going through the shortcut in the waterway in the Firelink Shrine after you unlock it.", "capra"));
        dataModels.add(new DataModel("Moonlight Butterfly", str2 + "Darkroot Garden", str3 + "Hard", "Drops: Soul of the Moonlight Butterfly, 6000 souls, 1 Humanity", "The Moonlight Butterfly is the boss of the first area of the Darkroot Garden. It can be found at the top of a moss covered staircase past the sleeping Stone Giants. The battle takes place on a long stone bridge with the Moonlight Butterfly flying next to it.", "moonlight"));
        dataModels.add(new DataModel("Gaping Dragon", str2 + "The Depths", str3 + "Easy", "Drops: 25,000 souls, 1 Twin Humanities, Blighttown Key, 1 Homeward Bone, Dragon King Greataxe (cut tail)", "The Gaping Dragon is the boss in the Depths, and gives you the key to the upper entrance to Blighttown.", "gaping"));
        dataModels.add(new DataModel("Chaos Witch Quelaag", str2 + "Blighttown", str3 + "Medium", "Drops: Soul of Quelaag, 20,000 souls, 1 Twin Humanities", "A half-woman, half-spider demon located in a small lair near the bottom of Blighttown. One of the Witch of Izalith's daughters, transformed into a monster by the flames of chaos. She guards the second Bell of Awakening.", "quelaag"));
        dataModels.add(new DataModel("Great Grey Wolf Sif", str2 + "Darkroot Garden", str3 + "Easy", "Drops: Soul of Sif, 40,000 souls, Covenant of Artorias, 1 Humanity, Blighttown Key, 1 Homeward Bone", " A very large wolf who resides in a serene graveyard, across the river from Darkroot Garden. It can also be reached by climbing up the ladder close to the Hydra in Darkroot Basin.", "sif"));
        dataModels.add(new DataModel("Stray Demon", str2 + "Undead Asylum", str3 + "Easy", "Drops: Titanite Slab, 20,000 souls, 1 Humanity, 1 Homeward Bone", "A large demon found after returning to the Northern Undead Asylum. This is actually the first demon you see in the game; as you exit your cell at the beginning he can be seen and heard stomping around to your right through the bars in the hallway. ", "stray"));
        dataModels.add(new DataModel("Iron Golem", str2 + "Sen's Fortress", str3 + "Easy", "Drops: Core of an Iron Golem, 40,000 souls, 1 Humanity", " The last trial of Sen's Fortress, the Iron Golem. The boss is located on a bridge at the top of the fortress, and must be defeated in order to reach Anor Londo.", "golem"));
        dataModels.add(new DataModel("Crossbreed Priscilla", str2 + "Painted World of Ariamis", str3 + "Easy", "Drops: Soul of Priscilla, 30,000 souls, 1 Twin Humanities, Priscilla's Dagger (cut tail)", "Half dragon boss found in Painted World of Ariamis. An optional boss, she wields a giant scythe and has the ability to turn invisible.", "priscilla"));
        dataModels.add(new DataModel("Ornstein and Smough", str2 + "Anor Londo", str3 + "Hard", "Drops: (Kill Ornstein last) Soul of Ornstein, Leo Ring, Humanity. (Kill Smough last) Soul of Smough, 1 Humanity", "Dragonslayer Ornstein is the captain of the Four Knights of Gwyn, and presumably, the leader of Gwyn's knights. Executioner Smough is the royal executioner of Anor Londo. He longs to be ranked with the Four Knights, but his cruelty, including using his victim's bones as his food's seasoning, forever denied him the position.", "oands"));
        dataModels.add(new DataModel("Dark Sun Gwyndolin", str2 + "Anor Londo", str3 + "Medium", "Drops: Soul of Gwyndolin, 40,000 souls", "The last born of Lord Gwyn, Gwyndolin, is the leader of the Blade of the Dark Moon covenant and the only remaining deity in Anor Londo.", "gwyndolin"));
        dataModels.add(new DataModel("Four Kings", str2 + "Abyss (New Londo Ruins)", str3 + "Medium", "Drops: Bequeathed Lord Soul Shard, 60,000 souls, 4 Humanity", "The wraith-like remnants of the four leaders of New Londo Ruins who fell to dark after being tempted with the art of lifedrain.", "fourkings"));
        dataModels.add(new DataModel("Ceaseless Discharge", str2 + "Demon Ruins", str3 + "Medium", "Drops: 20,000 souls, 1 Humanity, 1 Homeward Bone", "An imposing fire based boss found in the Demon Ruins. He is the younger brother of the Daughters of Chaos.", "ceaseless"));
        dataModels.add(new DataModel("Demon Firesage", str2 + "Demon Ruins", str3 + "Medium", "Drops: 20,000 Souls, 1 Humanity, Demon's Catalyst", "A large demon found at the end of the Demon Ruins. It very closely resembles the Stray Demon and Asylum Demon, though its behavior and abilities are much more closely related to the former of the two bosses.", "demonfiresage"));
        dataModels.add(new DataModel("Centipede Demon", str2 + "Demon Ruins", str3 + "Easy", "Drops: 40,000 souls, Orange Charred Ring, 1 Humanity, 1 Homeward Bone", " The Centipede Demon is a large bug creature that dwells in lava deep in the Demon Ruins. Its arms and tail are separate living centipedes.", "centipede"));
        dataModels.add(new DataModel("The Bed of Chaos", str2 + "Lost Izalith", str3 + "Hard", "Drops: Lord's Soul, 60,000 Souls, 1 Humanity", "In a futile attempt to prolong the Age of Fire, the Witch of Izalith tried to recreate the First Flame. The ritual was a failure and its power formed a bed of life which would become the source of all demons. The boss is located in a chamber below Lost Izalith, guarded by a Daughter of Chaos.", "bedchaos"));
        dataModels.add(new DataModel("Pinwheel", str2 + "The Catacombs", str3 + "Easy", "Drops: 3,000 Souls, Rite of Kindling, (One of its three masks) Mask of the Father, Mask of the Mother, Mask of the Child", " Pinwheel is the boss that lies in the depths of The Catacombs, and arguably the easiest boss in the entire game.", "pinwheel"));
        dataModels.add(new DataModel("Nito", str2 + "Tomb of Giants", str3 + "Medium", "Drops: Lord Soul, 60,000 Souls, 1 Humanity", "One of the the holders of the four Lord Souls. He resides within the Tomb of Giants, and is the leader of the Gravelord Servant covenant.", "nito"));
        dataModels.add(new DataModel("Seath", str2 + "Duke's Archive/Crystal Cave", str3 + "Medium", "Drops: Lord Soul, 60,000 Souls, 1 Humanity", "Seath, the albino dragon, betrayed his own kind out of jealousy; for unlike his brethren, Seath did not possess the stone scales of immortality.", "seath"));
        dataModels.add(new DataModel("Gwyn Lord of Cinder", str2 + "Kiln of the First Flame", str3 + "Hard", "Drops: Soul of Gwyn, Lord of Cinder, 70,000 Souls", "Gwyn, Lord of Cinder was a great warrior and God, once known as the Lord of Sunlight, who ended the Age of Ancients when he fought the dragons along with Gravelord Nito, the witch of Izalith, Seath The Scaleless, and his faithful knights.", "gwyn"));

        //DB Stuff
        DatabaseHandler db = new DatabaseHandler(this);

        //Reading Bosses
        Log.d("Reading: ", "Reading all bosses...");
        List<Boss> bosses = db.getAllBosses();

        for (Boss b : bosses) {
            String log = "Id: " + b.get_id() + " , Name: " + b.get_name() + " , Zone: " + b.get_zone() + " , Difficulty: " + b.get_difficulty() + " , Drops: " + b.get_drops() + " , Extrainfo: " + b.get_extrainfo() + " , Imatge: " + b.get_imagesrc();
            //Writing bosses to log
            Log.d("Boss: ", log);
            dataModels.add(new DataModel(b.get_name(), str2 + b.get_zone(), str3 + b.get_difficulty(), b.get_drops(), b.get_extrainfo(), b.get_imagesrc()));
        }

        //End DB Stuff

        adapter = new CustomAdapter(dataModels, getApplicationContext());
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                DataModel dataModel = dataModels.get(position);

                Intent i = new Intent(BossList.this, BossDescription.class);
                i.putExtra("name", dataModel.getName());
                i.putExtra("location", dataModel.getZone());
                i.putExtra("drop", dataModel.getDrops());
                i.putExtra("extrainfo", dataModel.getExtrainfo());
                i.putExtra("imgsrc", dataModel.getImagesrc());

                startActivity(i);

            }
        });
    }
}
